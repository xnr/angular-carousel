export default function ($resource, api) {
  'ngInject'

  let model = $resource(
      api.url + ':subreddit.json',
      null,
      {
        get: {
          method: 'GET',
          params: {
            subreddit: 'all'
          }
        }
      }
  );
  return model;
}