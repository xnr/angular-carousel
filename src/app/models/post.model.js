export default function($resource, api) {
  'ngInject';

  let model = $resource(
      api.url + ':sub/comments/:id/:post.json'
  );
  return model;
}