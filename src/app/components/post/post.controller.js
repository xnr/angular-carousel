export class PostController {
  constructor($stateParams, $state, PostService, SubService) {
    'ngInject';

    this.$stateParams = $stateParams;
    this.$state = $state;
    this.postService = PostService;
    this.subService = SubService;
    
    this.onInit();
  }
  
  onInit() {
    let options = {
      post: this.$stateParams.post,
      id: this.$stateParams.id,
      sub: this.$stateParams.sub
    };
    this.postService.get(options)
        .then(array => {
          let post = array[0].data.children[0].data;
          this.title = post.title;
          this.score = post.score;
          this.image = post.url.slice(0, 22) != 'https://www.reddit.com' ? post.url : false;
          this.selftext = post.selftext;
          this.author = post.author;
          this.created = post.created + '000';

          let comments = array[1].data.children;
          this.comments = comments.map(item => {
            return {
              body: item.data.body,
              score: item.data.score,
              author: item.data.author,
              created: item.data.created + '000'
            }
          })
        })
  }

  back() {
    this.$state.go('sub', this.subService.route);
  }
}