export class SubController {
  constructor($stateParams, NetService, SubService, api) {
    'ngInject'

    this.$stateParams = $stateParams;
    this.subreddit = this.$stateParams.subreddit || api.defSub;
    this.defThumbnail = api.thumbnail;
    this.netService = NetService;
    this.subService = SubService;
    
    this.onInit();
  }
  
  onInit() {
    this.subService.subreddit = this.subreddit;
    let options = {
      subreddit: this.subreddit
    };
    if (this.$stateParams.before) options.before = this.$stateParams.before;
    if (this.$stateParams.after) options.after = this.$stateParams.after;
    this.subService.route = options;
    this.netService.get(options)
        .then(data => {
          this.subService.after = data.after;
          this.subService.before = data.before;
          this.data = data.children.map(item => {
            return {
              created: item.data.created + '000',
              num_comments: item.data.num_comments,
              title: item.data.title,
              href: item.data.url,
              thumbnail: item.data.thumbnail == 'self' ? this.defThumbnail : item.data.thumbnail,
              link: generateLink(item.data.permalink),
              author: item.data.author
            }
          })
          this.subService.images = this.data.map(item => item.thumbnail);
        });
    function generateLink(permalink) {
      permalink = permalink.split('/');
      return [permalink[2], permalink[4], permalink[5]];
    }
  }
}