export default function() {
  'ngInject';

  let directive = {
    templateUrl: 'app/directives/carousel.html',
    scope: {
      images: '=',
      imageTemplate: '=',
      imageWidth: '@',
      count: '@',
      slideCount: '@',
      time: '@',
      animationTime: '@',
      controls: '=',
      auto: '='
    },
    controller: CarouselController,
    controllerAs: 'vm',
    bindToController: true
  }
  return directive;
}

class CarouselController {
  constructor($scope, $element, $timeout, $interval, $compile) {
    'ngInject';

    this.$scope = $scope;
    this.$element = $element;
    this.$timeout = $timeout;
    this.$compile = $compile;
    this.$interval = $interval;
    this.slide = 0;
    this.first = [];
    this.last = [];
    this.lock = false;
    this.extslides = angular.element(this.$element.find('div')[1]);

    this.onInit();
  }
  onInit() {
    this.content = this.$compile(this.imageTemplate)(this.$scope.$parent);
    this.$timeout(() => {
      this.$element.prepend(this.content);
    }, 0);

    this.slideshow();
  }
  slideshow() {
    if (this.auto) {
      this.next();
      this.$timeout(this.slideshow.bind(this), this.time);
    }
  }
  pause() {
    this.auto = !this.auto;
    this.slideshow();
  }
  prev() {
    if (this.lock) return;
    this.setPosition(this.slide, 'prev');
    this.setAnimation(this.slide - this.slideCount);
  }
  next() {
    if (this.lock) return;
    this.setPosition(this.slide, 'next');
    this.setAnimation(+this.slide + +this.slideCount);
  }
  setPosition(index, way) {
      if (!(way == "prev" && index < 0) && !(way == 'next' && index > this.lastIndex - this.count + 1)) {
        return;
      }
      this.slide = index > 0 ? index - this.lastIndex - 1 : this.lastIndex + index + 1;
  }
  setFakeImages() {
    this.extslides.css('left', -this.imageWidth * this.count + 'px');
    for (let i=0; i<this.count; i++) {
      this.first[i] = this.images[i];
      this.last[this.count - 1 - i] = this.images[this.lastIndex - i];
    }
  }
  setAnimation(position) {
    let fps = 50,
        count = Math.ceil(this.animationTime / (1000/fps)),
        slideStep = (this.slide - position) / count;
    this.lock = true;
    this.$timeout(() => this.lock = false, this.animationTime);
    this.$interval(() => {
      this.slide -= slideStep;
      this.extslides.css('left', (-this.imageWidth * (+this.count + this.slide)) + 'px');
    }, (1000/fps), count);
  }
}