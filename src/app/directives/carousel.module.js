import CarouselDirective from './carousel.directive';
import RepeatComplete from './repeat-complete.directive';

angular
    .module('carousel', [])
    .directive('customCarousel', CarouselDirective)
    .directive('repeatComplete', RepeatComplete);
