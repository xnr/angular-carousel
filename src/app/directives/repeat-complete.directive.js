export default function() {
  'ngInject';
  
  let directive = {
    require: '^^customCarousel',
    link: (scope, elem, attr, ctrl) => {
      if (scope.$last) {
        ctrl.lastIndex = scope.$index;
        ctrl.setFakeImages();
      }
    }
  }
  return directive;
}