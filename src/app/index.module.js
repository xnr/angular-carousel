
import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
//controllers
import { MainController } from './main/main.controller';
import { ViewController } from './main/view.controller';
import { FiltersController } from './main/filters.controller';
import { SubController } from './components/sub/sub.controller';
import { PostController } from './components/post/post.controller';
//services
import { SubService } from './services/sub.service';
import { NetService } from './services/net.service';
import { PostService } from './services/post.service';
//models
import { default as NetModel } from './models/net.model';
import PostModel from './models/post.model';
//filters
import CommentsFilter from './filters/comments.filter';
//constants
import './constants/api.constant';
//modules
import './directives/carousel.module';

angular
    .module('reddit', ['ngAnimate', 'ngResource', 'ngMaterial', 'ui.router', 'reddit.api', 'carousel'])
    .config(config)
    .config(routerConfig)
    .run(runBlock)
    .service('NetService', NetService)
    .factory('NetModel', NetModel)
    .service('PostService', PostService)
    .factory('PostModel', PostModel)
    .service('SubService', SubService)
    .controller('FiltersController', FiltersController)
    .controller('MainController', MainController)
    .controller('ViewController', ViewController)
    .controller('SubController', SubController)
    .controller('PostController', PostController)
    .filter('CommentsFilter', CommentsFilter);
