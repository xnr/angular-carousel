angular.module('reddit.api', [])
  .constant('api', {
    url: 'http://www.reddit.com/r/',
    thumbnail: 'https://pbs.twimg.com/profile_images/667516091330002944/wOaS8FKS.png',
    defSub: 'all'
  });