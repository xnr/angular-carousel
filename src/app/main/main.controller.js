export class MainController {
  constructor($state, SubService) {
    'ngInject';

    this.$state = $state;
    this.subService = SubService;

    this.onInit();
  }

  onInit() {
  }

  getPage(way) {
    let params = {
      subreddit: this.subService.subreddit
    }
    if (way == 'after') {
      params.after = this.subService.after;
      params.before = null;
    } else {
      params.after = null;
      params.before = this.subService.before;
    }
    this.$state.go('sub', params);
  }

  getSub() {
    this.subService.before = '';
    this.subService.after = '';
    this.getPage();
  }
}
