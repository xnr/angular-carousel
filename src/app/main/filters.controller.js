export class FiltersController {
  constructor($state, SubService, PostService) {
    'ngInject';

    this.$state = $state;
    this.subService = SubService;
    this.postService = PostService;
    this.imageTemplate = "<p>{{vm.subService.subreddit}}</p>";

    this.onInit();
  }

  onInit() {
  }
}