export default function() {
  return function(comments, score) {
    return comments ? comments.filter(item => item.score >= score) : false;
  }
}