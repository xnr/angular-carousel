export function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  'ngInject';
  $stateProvider
      .state('layout', {
        views: {
          'header': {
            templateUrl: 'app/main/header.html'
          },
          'filters': {
            templateUrl: 'app/main/filters.html',
            controller: 'FiltersController',
            controllerAs: 'vm'
          },
          'view': {
            templateUrl: 'app/main/view.html',
            controller: 'ViewController',
            controllerAs: 'vm'
          }
        }
      })
      .state('main', {
        parent: 'layout',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('sub', {
        url: '^/{subreddit}?{before}&{after}',
        parent: 'main',
        templateUrl: 'app/components/sub/sub.html',
        controller: 'SubController',
        controllerAs: 'vm'
      })
      .state('post', {
        url: '^/{sub}/comments/{id}/{post}',
        parent: 'layout',
        templateUrl: 'app/components/post/post.html',
        controller: 'PostController',
        controllerAs: 'vm'
      });

  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}
