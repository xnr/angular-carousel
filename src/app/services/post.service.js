export class PostService {
  constructor($mdToast, PostModel) {
    'ngInject';

    this.postModel = PostModel;
    this.$mdToast = $mdToast;
    this.filter = '';
  }

  get(options) {
    return this.postModel
        .query(options)
        .$promise
        .then(response => response)
        .catch(error => {
          this.$mdToast.show(
              this.$mdToast.simple()
                  .textContent('Net error')
                  .hideDelay(3000)
          );
          return error;
        });
  }
}