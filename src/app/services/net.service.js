export class NetService {
  constructor($mdToast, NetModel) {
    'ngInject';

    this.netModel = NetModel;
    this.$mdToast = $mdToast;
    this.count = 0;
  }

  get(options) {
    if (options.after) {
      this.count = (this.count % 25) ? this.count - 1 : this.count + 25;
    } else if (options.before) {
      this.count = (this.count % 25) ? this.count - 25 : this.count + 1;
    } else {
      this.count = 0;
    }
    return this.netModel
        .get({
          subreddit: options.subreddit,
          count: this.count,
          after: options.after,
          before: options.before
        })
        .$promise
        .then(response => response.data)
        .catch(error => {
          this.$mdToast.show(
              this.$mdToast.simple()
                  .textContent('Net error' + error.statusText)
                  .hideDelay(3000)
          );
          return error;
        });
  }
}